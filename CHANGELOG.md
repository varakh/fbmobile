# CHANGELOG

## 1.6.5+23 - UNRELEASED
* ...

## 1.6.4+22 - 2024/11/01
* Dependency updates
* Internal build updates

## 1.6.3+21
* Fixed not receiving share requests from other applications

## 1.6.2+20
* Updated internal dependencies
* Moved progress indicator of _Show Configuration_ into the underlying button
* Bumped Android minSdk to `30` (Android 11)
* Bumped Android targetSdk to `34` (Android 14)
* Fixed permission service not handling Android SDK 33 correctly
* Fixed permission service not being started during application start

## 1.6.1+19
* Updated internal dependencies

## 1.6.0+18
* Fixed input colors in login view when using dark theme
* Added removal of individual files selected for upload
* Added size for individual files selected for upload
* Replaced intent sharing library with `flutter_sharing_intent`
* Added proper linting to project

## 1.5.1+17
* Fixed white background button in AppBar when light theme enabled
* Cleaned up login screen and added icon

## 1.5.0+16
* Switched to Material You defaulting to blue swatch colors respecting dark mode
* Switched to Material You navigation bar and removed unsupported swipe navigation
* Increased target SDK to `33`
* Increased dart to `>= 2.18.5`
* Indicate configuration loading in profile view
* Switched linked git repository away from GitHub
* Updated internal dependencies

## 1.4.2+15
* Minor cleanup
* Added external drone CI
* Updated to Android embedding v2
* Updated to Gradle 7
* Upgraded internal dependencies to latest versions

## 1.4.1+14
* Fixed opening links

## 1.4.0+13
* Increased target SDK to `30`
* Upgraded to Dart `2.15.1`
* Upgraded to use null-safety
* Upgraded internal dependencies to latest versions
* Replaced `share` with `share_plus`

## 1.3.3+12
* Automatically switch to initial tab when coming from the share menu
* Upgraded internal dependencies

## 1.3.2+11
* Add a slash to copied URLs
* Expand item when pressing on the card's title in history view

## 1.3.1+10
* Added gesture detection for tab bar
* Disable upload when no files have been attached or upload text input is empty
* Added version information to about view
* Minor refactor regarding state management

## 1.3.0+9
* Allow API key login
* Revamp profile view
* Adapt color of tab bar text and use outlined icons when active
* Suffix the API key comment with UNIX timestamp when credential login is used
* Fixed an error when logging out and logging back in again in the history view
* Minor code refactor

## 1.2.2+8
* Adapt status bar color to match app's theme

## 1.2.1+7
* Improve visual differences between the Upload tab and the upload button
* Improved visuals in bottom tab bar
* Updated dependencies and build with Flutter > 2 for the first time
* Updated to new button styles for New Material theme proposed by Google

## 1.2.0+6
* Only copy multipaste link if multi box checked
* Add copy to clipboard shortcut in history view

## 1.1.0+5
* Replace API key login with username and password login: a valid API key will automatically be created on login

## 1.0.0+3 and 1.0.0+4 
* Fixed launch bug related to Android 4.0.1 gradle plugin

## 1.0.0+2
* Automatic refresh history if something has been uploaded

## 1.0.0+1
* Initial release
