import 'package:fbmobile/core/services/permission_service.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../locator.dart';
import '../../ui/views/home_view.dart';
import '../enums/viewstate.dart';
import '../services/navigation_service.dart';
import '../services/session_service.dart';
import 'base_model.dart';

class StartUpViewModel extends BaseModel {
  final SessionService _sessionService = locator<SessionService>();
  final PermissionService _permissionService = locator<PermissionService>();
  final NavigationService _navigationService = locator<NavigationService>();

  Future handleStartUpLogic() async {
    setStateView(ViewState.busy);
    setStateMessage(translate('startup.init'));
    await Future.delayed(const Duration(milliseconds: 100));

    setStateMessage(translate('startup.start_services'));
    await _sessionService.start();
    await _permissionService.start();
    await Future.delayed(const Duration(milliseconds: 100));

    _navigationService.navigateAndReplaceTo(HomeView.routeName);

    setStateView(ViewState.idle);
  }
}
