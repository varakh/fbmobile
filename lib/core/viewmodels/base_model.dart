import 'package:flutter/widgets.dart';
import 'package:logger/logger.dart';

import '../../core/util/logger.dart';
import '../enums/viewstate.dart';

class BaseModel extends ChangeNotifier {
  static const String stateViewKey = 'viewState';
  static const String stateMessageKey = 'viewMessage';

  final Logger _logger = getLogger();

  bool _isDisposed = false;

  final Map<String, Object?> _stateMap = {
    stateViewKey: ViewState.idle,
    stateMessageKey: null
  };

  ViewState? get state => _stateMap[stateViewKey] as ViewState?;

  String? get stateMessage => _stateMap[stateMessageKey] as String?;

  bool getStateValueAsBoolean(String key) {
    if (_stateMap.containsKey(key) && _stateMap[key] is bool) {
      return _stateMap[key] as bool;
    }

    return false;
  }

  String? getStateValueAsString(String key) {
    if (_stateMap.containsKey(key) && _stateMap[key] is String) {
      return _stateMap[key] as String;
    }

    return null;
  }

  int? getStateValueAsInt(String key) {
    if (_stateMap.containsKey(key) && _stateMap[key] is int) {
      return _stateMap[key] as int;
    }

    return null;
  }

  void setStateBoolValue(String key, bool stateValue) =>
      _setStateValue(key, stateValue);

  void setStateIntValue(String key, int? stateValue) =>
      _setStateValue(key, stateValue);

  void setStateStringValue(String key, String? stateValue) =>
      _setStateValue(key, stateValue);

  void _setStateValue(String key, Object? stateValue) {
    if (_stateMap.containsKey(key)) {
      _stateMap.update(key, (value) => stateValue);
    } else {
      _stateMap.putIfAbsent(key, () => stateValue);
    }

    if (!_isDisposed) {
      notifyListeners();
      _logger
          .d("Notified state value update '($key, ${stateValue.toString()})'");
    }
  }

  void removeStateValue(String key) {
    _stateMap.remove(key);

    if (!_isDisposed) {
      notifyListeners();
      _logger.d("Notified state removal of '$key'");
    }
  }

  void setStateView(ViewState stateView) {
    _setStateValue(stateViewKey, stateView);
  }

  void setStateMessage(String? stateMessage) {
    _setStateValue(stateMessageKey, stateMessage);
  }

  @override
  void dispose() {
    _logger.d("Calling dispose");
    super.dispose();
    _isDisposed = true;
  }
}
