import 'package:package_info_plus/package_info_plus.dart';

import '../../core/services/link_service.dart';
import '../../locator.dart';
import '../enums/viewstate.dart';
import 'base_model.dart';

class AboutModel extends BaseModel {
  final LinkService _linkService = locator<LinkService>();

  PackageInfo packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  void init() async {
    await _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    setStateView(ViewState.busy);
    final PackageInfo info = await PackageInfo.fromPlatform();
    packageInfo = info;
    setStateView(ViewState.idle);
  }

  void openLink(String link) {
    _linkService.open(link);
  }
}
