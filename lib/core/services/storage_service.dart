import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../models/session.dart';

class StorageService {
  static const _sessionKey = 'session';
  static const _lastUrlKey = 'last_url';

  Future<bool> storeLastUrl(String url) {
    return _store(_lastUrlKey, url);
  }

  Future<String?> retrieveLastUrl() async {
    return await _retrieve(_lastUrlKey);
  }

  Future<bool> hasLastUrl() async {
    return await _exists(_lastUrlKey);
  }

  Future<bool> storeSession(Session session) {
    return _store(_sessionKey, json.encode(session));
  }

  Future<Session> retrieveSession() async {
    var retrieve = await _retrieve(_sessionKey);
    return Session.fromJson(json.decode(retrieve!));
  }

  Future<bool> hasSession() {
    return _exists(_sessionKey);
  }

  Future<bool> removeSession() {
    return _remove(_sessionKey);
  }

  Future<bool> _exists(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }

  Future<bool> _remove(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }

  Future<String?> _retrieve(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  Future<bool> _store(String key, String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, value);
  }
}
