import '../enums/error_code.dart';
import 'service_exception.dart';

class RestServiceException extends ServiceException {
  final int statusCode;
  final dynamic responseBody;

  RestServiceException(this.statusCode, {this.responseBody, super.message = null})
      : super(code: ErrorCode.restError);

  @override
  String toString() {
    return "$code $statusCode $message";
  }
}
