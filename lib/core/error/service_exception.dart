import '../enums/error_code.dart';

class ServiceException implements Exception {
  final ErrorCode code;
  final String? message;

  ServiceException({this.code = ErrorCode.generalError, this.message = ''});

  @override
  String toString() {
    return "$code: $message";
  }
}
