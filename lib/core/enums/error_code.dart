/// Enums for error codes
enum ErrorCode {
  /// A generic error
  generalError,

  /// Errors related to connections
  socketError,
  socketTimeout,

  /// A REST error (response code wasn't 200 or 204)
  restError,

  /// Custom errors
  invalidApiKey
}
