import 'package:json_annotation/json_annotation.dart';

import 'history_multipaste_item_entry.dart';

part 'history_multipaste_item.g.dart';

@JsonSerializable()
class HistoryMultipasteItem {
  final String date;
  final Map<String, HistoryMultipasteItemEntry> items;

  @JsonKey(name: "url_id")
  final String urlId;

  HistoryMultipasteItem(this.items, {required this.date, required this.urlId});

  // JSON Init
  factory HistoryMultipasteItem.fromJson(Map<String, dynamic> json) =>
      _$HistoryMultipasteItemFromJson(json);

  // JSON Export
  Map<String, dynamic> toJson() => _$HistoryMultipasteItemToJson(this);
}
