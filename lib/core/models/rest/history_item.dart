import 'package:json_annotation/json_annotation.dart';

part 'history_item.g.dart';

@JsonSerializable()
class HistoryItem {
  @JsonKey(required: true)
  final String id;
  final String date;
  final String filename;
  final String filesize;
  final String hash;
  final String mimetype;
  final String? thumbnail;

  HistoryItem(
      {required this.date,
      required this.filename,
      required this.filesize,
      required this.hash,
      required this.id,
      required this.mimetype,
      this.thumbnail});

  // JSON Init
  factory HistoryItem.fromJson(Map<String, dynamic> json) =>
      _$HistoryItemFromJson(json);

  // JSON Export
  Map<String, dynamic> toJson() => _$HistoryItemToJson(this);
}
