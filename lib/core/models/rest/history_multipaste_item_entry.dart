import 'package:json_annotation/json_annotation.dart';

part 'history_multipaste_item_entry.g.dart';

@JsonSerializable()
class HistoryMultipasteItemEntry {
  final String id;

  HistoryMultipasteItemEntry({required this.id});

  // JSON Init
  factory HistoryMultipasteItemEntry.fromJson(Map<String, dynamic> json) =>
      _$HistoryMultipasteItemEntryFromJson(json);

  // JSON Export
  Map<String, dynamic> toJson() => _$HistoryMultipasteItemEntryToJson(this);
}
