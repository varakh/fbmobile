import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../core/enums/viewstate.dart';
import '../../core/viewmodels/home_model.dart';
import '../widgets/my_appbar.dart';
import 'base_view.dart';

class HomeView extends StatelessWidget {
  static const routeName = '/home';

  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return BaseView<HomeModel>(
      builder: (context, model, child) => Scaffold(
          appBar: MyAppBar(title: Text(translate('app.title'))),
          body: model.state == ViewState.busy
              ? const Center(child: CircularProgressIndicator())
              : Container()),
    );
  }
}
