import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../core/enums/viewstate.dart';
import '../../core/viewmodels/startup_model.dart';

class StartUpView extends StatelessWidget {
  static const routeName = '/';

  const StartUpView({super.key});

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StartUpViewModel>.reactive(
        viewModelBuilder: () => StartUpViewModel(),
        onViewModelReady: (model) => model.handleStartUpLogic(),
        builder: (context, model, child) => Scaffold(
            body: model.state == ViewState.busy
                ? Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                        const CircularProgressIndicator(),
                        (model.stateMessage!.isNotEmpty
                            ? Text(model.stateMessage!)
                            : Container())
                      ]))
                : Container()));
  }
}
