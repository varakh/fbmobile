import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../../core/enums/viewstate.dart';
import '../../core/models/session.dart';
import '../../core/viewmodels/profile_model.dart';
import '../shared/app_colors.dart';
import '../shared/text_styles.dart';
import '../shared/ui_helpers.dart';
import '../widgets/my_appbar.dart';
import 'base_view.dart';

class ProfileView extends StatelessWidget {
  static const routeName = '/profile';

  const ProfileView({super.key});

  @override
  Widget build(BuildContext context) {
    return BaseView<ProfileModel>(
        builder: (context, model, child) => Scaffold(
            appBar: MyAppBar(title: Text(translate('titles.profile'))),
            body: _render(model, context)));
  }

  Widget _render(ProfileModel model, BuildContext context) {
    var url = Provider.of<Session>(context).url;
    var apiKey = Provider.of<Session>(context).apiKey;

    return model.state == ViewState.busy
        ? const Center(child: CircularProgressIndicator())
        : ListView(
            children: <Widget>[
              UIHelper.verticalSpaceMedium(),
              Padding(
                  padding: const EdgeInsets.only(left: 25.0),
                  child: Center(
                      child: Text(
                    translate('profile.instance'),
                    style: subHeaderStyle,
                  ))),
              UIHelper.verticalSpaceMedium(),
              Padding(
                  padding: const EdgeInsets.only(left: 25.0),
                  child: Center(
                      child: Linkify(
                    onOpen: (link) => model.openLink(link.url),
                    text: translate('profile.connection', args: {'url': url}),
                    options: const LinkifyOptions(humanize: false),
                  ))),
              UIHelper.verticalSpaceMedium(),
              Padding(
                  padding: const EdgeInsets.only(left: 25.0, right: 25.0),
                  child: ElevatedButton.icon(
                      icon: model.configLoading
                          ? Container(
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(2.0),
                              child: const CircularProgressIndicator(
                                color: blueColor,
                                strokeWidth: 3,
                              ),
                            )
                          : const Icon(Icons.settings, color: blueColor),
                      label: Text(
                        model.configLoading
                            ? translate('profile.show_config_loading')
                            : translate('profile.show_config'),
                      ),
                      onPressed: () async {
                        await model.showConfig(url);
                      })),
              UIHelper.verticalSpaceMedium(),
              Padding(
                  padding: const EdgeInsets.only(left: 25.0, right: 25.0),
                  child: ElevatedButton.icon(
                      icon: const Icon(Icons.lock, color: orangeColor),
                      label: Text(
                        translate('profile.reveal_api_key'),
                      ),
                      onPressed: () {
                        model.revealApiKey(apiKey);
                      })),
              UIHelper.verticalSpaceMedium(),
              Padding(
                  padding: const EdgeInsets.only(left: 25.0, right: 25.0),
                  child: ElevatedButton.icon(
                      icon: const Icon(Icons.exit_to_app, color: redColor),
                      label: Text(
                        translate('profile.logout'),
                      ),
                      onPressed: () async {
                        await model.logout();
                      })),
            ],
          );
  }
}
