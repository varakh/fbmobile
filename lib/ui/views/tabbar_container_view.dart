import 'package:fbmobile/ui/views/login_view.dart';
import 'package:fbmobile/ui/views/navbar_authenticated.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../core/models/session.dart';

class TabBarContainerView extends StatelessWidget {
  const TabBarContainerView({super.key});

  @override
  Widget build(BuildContext context) {
    Session? currentSession = Provider.of<Session?>(context);
    bool isAuthenticated =
        currentSession != null ? currentSession.apiKey.isNotEmpty : false;

    if (isAuthenticated) {
      return const AuthenticatedNavBarView();
    }

    return LoginView();
  }
}
