import 'package:fbmobile/core/util/logger.dart';
import 'package:fbmobile/ui/views/profile_view.dart';
import 'package:fbmobile/ui/views/upload_view.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

import '../shared/app_colors.dart';
import 'history_view.dart';

class AuthenticatedNavBarView extends StatefulWidget {
  const AuthenticatedNavBarView({super.key});

  @override
  AuthenticatedNavBarState createState() => AuthenticatedNavBarState();
}

class AuthenticatedNavBarState extends State<AuthenticatedNavBarView>
    with SingleTickerProviderStateMixin {
  final Logger _logger = getLogger();
  int _currentTabIndex = 0;

  void updateIndex(int targetIndex) {
    setState(() {
      _currentTabIndex = targetIndex;
      _logger.d("Changing current tab index to '$targetIndex'");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavigationBar(
        key: UniqueKey(),
        onDestinationSelected: (int index) {
          updateIndex(index);
        },
        selectedIndex: _currentTabIndex,
        labelBehavior: NavigationDestinationLabelBehavior.alwaysHide,
        destinations: const <Widget>[
          NavigationDestination(
            icon: Icon(Icons.upload_outlined),
            label: 'Upload',
          ),
          NavigationDestination(
            icon: Icon(Icons.history_outlined),
            label: 'History',
          ),
          NavigationDestination(
            icon: Icon(Icons.person_outlined),
            label: 'Profile',
          ),
        ],
      ),
      body: <Widget>[
        Container(
          color: myColor,
          alignment: Alignment.center,
          child: const UploadView(),
        ),
        Container(
          color: myColor,
          alignment: Alignment.center,
          child: const HistoryView(),
        ),
        Container(
          color: myColor,
          alignment: Alignment.center,
          child: const ProfileView(),
        ),
      ][_currentTabIndex],
    );
  }
}
