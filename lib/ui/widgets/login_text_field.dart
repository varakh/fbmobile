import 'package:flutter/material.dart';

class LoginTextField extends StatelessWidget {
  final TextEditingController controller;
  final String placeHolder;
  final TextInputType keyboardType;
  final bool obscureText;
  final Widget prefixIcon;

  const LoginTextField(this.controller, this.placeHolder, this.prefixIcon,
      {super.key,
      this.keyboardType = TextInputType.text,
      this.obscureText = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      height: 50.0,
      alignment: Alignment.centerLeft,
      child: TextFormField(
          keyboardType: keyboardType,
          obscureText: obscureText,
          decoration: InputDecoration(
            suffixIcon: IconButton(
              onPressed: () => controller.clear(),
              icon: const Icon(Icons.clear),
            ),
            prefixIcon: prefixIcon,
            hintText: placeHolder,
            contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          controller: controller),
    );
  }
}
