import 'package:flutter/material.dart';

import '../../core/services/navigation_service.dart';
import '../../locator.dart';
import '../../ui/views/about_view.dart';

class AboutIconButton extends StatelessWidget {
  AboutIconButton({super.key});

  final NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: const Icon(Icons.help),
        onPressed: () {
          _navigationService.navigateTo(AboutView.routeName);
        });
  }
}
