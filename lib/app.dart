import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';

import 'core/enums/refresh_event.dart';
import 'core/manager/dialog_manager.dart';
import 'core/manager/lifecycle_manager.dart';
import 'core/models/session.dart';
import 'core/services/dialog_service.dart';
import 'core/services/navigation_service.dart';
import 'core/services/refresh_service.dart';
import 'core/services/session_service.dart';
import 'locator.dart';
import 'ui/app_router.dart';
import 'ui/shared/app_colors.dart';
import 'ui/views/startup_view.dart';

class MyApp extends StatelessWidget {
  static final _defaultLightColorScheme = ColorScheme.fromSwatch(
      primarySwatch: myColor, brightness: Brightness.light);
  static final _defaultDarkColorScheme = ColorScheme.fromSwatch(
      primarySwatch: myColor, brightness: Brightness.dark);

  MyApp({super.key}) {
    initializeDateFormatting('en');
  }

  @override
  Widget build(BuildContext context) {
    var localizationDelegate = LocalizedApp.of(context).delegate;

    return LocalizationProvider(
        state: LocalizationProvider.of(context).state,
        child: StreamProvider<RefreshEvent?>(
            initialData: null,
            create: (context) =>
                locator<RefreshService>().refreshEventController.stream,
            child: StreamProvider<Session?>(
              initialData: Session.initial(),
              create: (context) =>
                  locator<SessionService>().sessionController.stream,
              child: LifeCycleManager(child: DynamicColorBuilder(
                  builder: (lightColorScheme, darkColorScheme) {
                return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  title: translate('app.title'),
                  builder: (context, child) => Navigator(
                    key: locator<DialogService>().dialogNavigationKey,
                    onGenerateRoute: (settings) => MaterialPageRoute(
                        builder: (context) => DialogManager(child: child)),
                  ),
                  theme: ThemeData(
                      useMaterial3: true,
                      brightness: Brightness.light,
                      colorScheme:
                          lightColorScheme ?? _defaultLightColorScheme),
                  darkTheme: ThemeData(
                      useMaterial3: true,
                      colorScheme: darkColorScheme ?? _defaultDarkColorScheme),
                  onGenerateRoute: AppRouter.generateRoute,
                  navigatorKey: locator<NavigationService>().navigationKey,
                  home: const StartUpView(),
                  supportedLocales: localizationDelegate.supportedLocales,
                  locale: localizationDelegate.currentLocale,
                );
              })),
            )));
  }
}
